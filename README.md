# CS 373: Software Engineering - 11am Group 12 - **ConservOcean**

- Website: https://conservocean.me
- Git SHA: bce1ea34c62397f14f67c2ab134c103a7e89f9d8
- GitLab Pipelines: https://gitlab.com/joewallery/cs373-group12/-/pipelines

| Name                            | EID     | GitLab ID      | Estimated Time (hours)                       | Actual Time (hours)                          |
| ------------------------------- | ------- | -------------- | -------------------------------------------- | -------------------------------------------- |
| Joseph Wallery (Phase 1)        | jbw2756 | @joewallery    | **I:** 15<br>**II:** 25<br>**III:**<br>**IV:**  | **I:** 28<br>**II:**<br>**III:**<br>**IV:**  |
| Andy Weng                       | aw33252 | @AndyWeng33252 | **I:** 20<br>**II:**<br>**III:**<br>**IV:**  | **I:** 22<br>**II:**<br>**III:**<br>**IV:**  |
| Christine Tsou                  | cjt2538 | @ChristineTsou | **I:** 15<br>**II:**<br>**III:**<br>**IV:**  | **I:** 16<br>**II:**<br>**III:**<br>**IV:**  |
| Rishi Salem (Phase 2)           | ras5832 | @rishi312      | **I:** 15<br>**II:**<br>**III:**<br>**IV:**  | **I:** 20<br>**II:**<br>**III:**<br>**IV:**  |
| Serena Zamarripa                | snz252  | @renaz6        | **I:** 11<br>**II:**<br>**III:**<br>**IV:**  | **I:** 21<br>**II:**<br>**III:**<br>**IV:**  |
| Dane Strandboge                 | des2923 | @DStrand1      | **I:** N/A<br>**II:**<br>**III:**<br>**IV:** | **I:** N/A<br>**II:**<br>**III:**<br>**IV:** |

- Comments:
